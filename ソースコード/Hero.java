import java.util.*;

public class Hero {
        Main main1 = new Main();
        String name = "勇者" + main1.input3;  //Mainで入力した名前の反映

        int hp = 100;  //勇者の初期HP
        final int MAX_HP = 100;  //勇者の最大HP

        int baseAttack = 5;  //勇者の基本攻撃力
        Scanner scan1 = new Scanner(System.in);

        //スライム戦
        public void attackHS(Monster m){
                System.out.println("---------------------------------------");
                System.out.println(this.name + "の攻撃！");
                System.out.println("---------------------------------------");
                /*スライムへの攻撃力算出*/
                int atRanHS = new java.util.Random().nextInt(3);
                int atHS = baseAttack + atRanHS;
                m.hp1 -= atHS;

                scan1.nextLine();
                System.out.println("---------------------------------------");
                System.out.println(m.name1 + "に"  + atHS + "ポイントのダメージを与えた!");
                System.out.println(m.name1 + "の現在HP:" + m.hp1);
                System.out.println("---------------------------------------");

                scan1.nextLine();
                if(m.hp1 <= 0){
                        System.out.println("---------------------------------------");
                        System.out.println(m.name1 + "を倒した！");
                        System.out.println("戦闘勝利！");
                        System.out.println("---------------------------------------");
                }
        }

        //ウィッチ戦
        public void attackHW(Monster m){
                System.out.println("---------------------------------------");
                System.out.println(this.name + "の攻撃！");
                System.out.println("---------------------------------------");
    /*ウィッチへの攻撃力算出*/
                int atRanHW = new java.util.Random().nextInt(3);
                int atHW = baseAttack + atRanHW;
                m.hp2 -= atHW;

                scan1.nextLine();
                System.out.println("---------------------------------------");
                System.out.println(m.name2 + "に" + atHW + "ポイントのダメージを与えた!");
                System.out.println(m.name2 + "の現在HP:" + m.hp2);
                System.out.println("---------------------------------------");

                scan1.nextLine();
                if(m.hp2 <= 0){
                        System.out.println("---------------------------------------");
                        System.out.println(m.name2 + "を倒した！");
                        System.out.println("戦闘勝利！");

                        System.out.println("---------------------------------------");
                }
        }

        //魔王戦
        public void attackHB(Boss b) {
                System.out.println("---------------------------------------");
                System.out.println(this.name + "の攻撃！");
                System.out.println("---------------------------------------");
                /*魔王への攻撃力算出*/
                int atRanHB = new java.util.Random().nextInt(3);
                int atHB = baseAttack + atRanHB;
                b.hp -= atHB;

                scan1.nextLine();
                System.out.println("---------------------------------------");

                System.out.println(b.name + "に" + atHB + "ポイントのダメージを与えた!");
                System.out.println(b.name + "の現在HP:" + b.hp);
                System.out.println("---------------------------------------");

                scan1.nextLine();
    			if(b.hp <= 0){
                        System.out.println("---------------------------------------");
                        System.out.println(b.name + "を倒した！");
                        System.out.println("戦闘勝利！");
                        System.out.println("---------------------------------------");
                }
        }

        //逃げるコマンド
        public void run() {
                System.out.println("---------------------------------------");
                System.out.println(this.name + "は、逃げ出した！");
                System.out.println("---------------------------------------");
        }
}