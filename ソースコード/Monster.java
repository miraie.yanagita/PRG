import java.util.*;

public class Monster {
        Main main = new Main();
        String heroName = "勇者" + main.input3;  //Mainで入力した名前の反映

        String name1 = "スライム";
        String name2 = "ウィッチ";

        int hp1 = 10;  //スライムの初期HP
        int hp2 = 15;  //ウィッチの初期HP
        final int MAX_HP1 = 100;  //スライムの最大HP
        final int MAX_HP2 = 100;  //ウィッチの最大HP

        int baseAttackSL = 5;  //スライムの基本攻撃力
        int baseAttackWI = 6;  //ウィッチの基本攻撃力

        Scanner scan1 = new Scanner(System.in);

        //スライムの攻撃
        public void attackSL(Hero h) {
                if (hp1 >0){
                        System.out.println("---------------------------------------");
                        System.out.println(this.name1 + "の攻撃！");
                        System.out.println("---------------------------------------");
                        /*スライムの攻撃力算出*/
                        int atRanSL = new java.util.Random().nextInt(3);
                        int atSL = baseAttackSL + atRanSL;
                        h.hp -= atSL;

                        scan1.nextLine();
                        System.out.println("---------------------------------------");
                        System.out.println(this.heroName + "に" + atSL + "ポイントのダメージを与えた!");
                        System.out.println(this.heroName + "の現在HP:" + h.hp);
                        System.out.println("---------------------------------------");
                }

                scan1.nextLine();
                if(h.hp <= 0){
                        System.out.println("---------------------------------------");
                        System.out.println(this.heroName + "は力尽きた…");
                        System.out.println("GAME OVER");
                        System.out.println("---------------------------------------");
                        System.exit(0);
                }
        }

        //ウィッチの攻撃
        public void attackWI(Hero h) {
                if (hp2 > 0){
                        System.out.println("---------------------------------------");
                        System.out.println(this.name2 + "の魔法攻撃！");
                        System.out.println("---------------------------------------");
                        /*ウィッチの攻撃力算出*/
                        int atRanWI = new java.util.Random().nextInt(3);
                        int atWI = baseAttackWI + atRanWI;
                        h.hp -= atWI;

                        scan1.nextLine();
                        System.out.println("---------------------------------------");
                        System.out.println(this.heroName + "に" + atWI + "ポイントのダメージを与えた!");
                        System.out.println(this.heroName + "の現在HP:" + h.hp);
                        System.out.println("---------------------------------------");
                }

                scan1.nextLine();
                if(h.hp <= 0){
                        System.out.println("---------------------------------------");
                        System.out.println(this.heroName + "は力尽きた…");
                        System.out.println("GAME OVER");
                        System.out.println("---------------------------------------");
                        System.exit(0);
                }
        }
}